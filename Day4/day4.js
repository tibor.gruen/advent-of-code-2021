var fs = require('fs');
var math = require('mathjs');
var zipwith = require("zipwith").zipwith;

winning_boards = [
  [
  1, 1, 1, 1, 1,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0
  ],
  [
  0, 0, 0, 0, 0,
  1, 1, 1, 1, 1,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0
  ],
  [
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  1, 1, 1, 1, 1,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0
  ],
  [
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  1, 1, 1, 1, 1,
  0, 0, 0, 0, 0
  ],
  [
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  1, 1, 1, 1, 1
  ],
  [
  1, 0, 0, 0, 0,
  1, 0, 0, 0, 0,
  1, 0, 0, 0, 0,
  1, 0, 0, 0, 0,
  1, 0, 0, 0, 0,
  ],
  [
  0, 1, 0, 0, 0,
  0, 1, 0, 0, 0,
  0, 1, 0, 0, 0,
  0, 1, 0, 0, 0,
  0, 1, 0, 0, 0,
  ],
  [
  0, 0, 1, 0, 0, 
  0, 0, 1, 0, 0, 
  0, 0, 1, 0, 0, 
  0, 0, 1, 0, 0, 
  0, 0, 1, 0, 0
  ],
  [
  0, 0, 0, 1, 0,
  0, 0, 0, 1, 0,
  0, 0, 0, 1, 0,
  0, 0, 0, 1, 0,
  0, 0, 0, 1, 0,
  ],
  [
  0, 0, 0, 0, 1, 
  0, 0, 0, 0, 1, 
  0, 0, 0, 0, 1, 
  0, 0, 0, 0, 1, 
  0, 0, 0, 0, 1
  ]
];

function arrayEquals(a, b)
{
  return Array.isArray(a) &&
    Array.isArray(b) &&
           a.length === b.length &&
                a.every((val, index) => val === b[index]);
}
  
function unmarked( z )
{
  if (z.im == 0) {
    return z.re;
  }
  else {
    return 0;
  }
}

const sumreducer = (accumulator, curr) => accumulator + curr;
const minreducer = function (accumulator, curr) {
  if (curr < accumulator) {
    return curr;
  }
  else
  {
    return accumulator;
  }
}
const maxreducer = function (accumulator, curr) {
  if (curr > accumulator) {
    return curr;
  }
  return accumulator;
}

function calculate_score( complex_board, draw )
{
  return complex_board
  .map( z => unmarked(z) )
  .reduce(sumreducer)
  * draw;
}

function turns_from_board( complex_board ) {
  return (-1) * complex_board
  .map( x => math.im(x) )
  .reduce( minreducer);
}

function draw_from_board( complex_board ) {

}

var random_order = [];
var complex_boards = [];
var part1_boards = [];
var part2_boards = [];
var winner = -1;
var score = 0;
var turns = 0;
var last_draw = 0;
var last_winner = -1;
var last_score = 0;
var last_turns = 0;
var last_complex_board = [];

fs.readFile('input.txt', 'utf8', 
//fs.readFile('sample.txt', 'utf8', 
  function(err, data)
  {
    if (err) {
      throw err;
    }
    //console.log(data.split());
    random_order = data
      .split('\r\n',limit=1)
      .join()
      .split(',')
      .map(Number);
    //console.log(random_order);
    var boards = data.split('\r\n\r\n');
    boards.shift();
    //console.log(boards);
    boards.forEach(function(board, player, array)
    {
      board_lines = board
        .replace(/\s+/g, ' ')
        .split('\r\n');
      //console.log(board_lines);
      var real_board = board_lines
        .join(' ')
        .trim()
        .split(' ')
        .map(Number);
      //console.log(real_board);
      part1_boards[player] = real_board
      .map( x => math.complex(x,0));
      part2_boards[player] = real_board
      .map( x => math.complex(x,0));
      //complex_boards[player] = real_board
      //  .map( x => math.complex(x,0));
    });
    determine_winner( random_order, part1_boards );
    console.log('Part 1: Board %d wins after %d turns. The score is %d.', winner, turns, score);
    determine_last_winner( random_order, part2_boards );
    console.log('Part 2: Player %d wins last after %d turns. The score is %d.', last_winner+1, last_turns, last_score );
    console.log('Board of last winner %d:', last_winner+1);
    console.log(last_complex_board.map( x => math.im(x)));
});

function determine_winner( random_order, complex_boards )
{
  var gameover = false;
  random_order.every(function( draw, turn, array )
  {
    //console.log('The draw for turn %d was %d', turn+1, draw);
    complex_boards.forEach(function( complex_board, player )
    {
      var index = complex_board
      .map( x => math.re(x) )
      .findIndex( el => el == draw );
      if (index !== -1) { 
        //console.log('Player %d has marked number %d at index %d.', player+1, draw, index);
        complex_board[index] = math.add(complex_board[index], math.i);
        var marked_board = complex_board
        .map( x => math.im(x) );
        //console.log(complex_board.map( x => math.re(x) ));
        //console.log(marked_board);
        winning_boards.forEach(function( winboard )
        {
          //console.log(winboard);
          var checkboard = zipwith((a, b) => a * b, marked_board, winboard);
          if ( arrayEquals( checkboard, winboard ) )
          {
            score = calculate_score( complex_board, draw );
            winner = player+1;
            turns = turn+1;
            gameover = true;
          }
        });
      }
    });
    return !gameover;
  });
}

function determine_last_winner( random_order, complex_boards ) {
  random_order.forEach(function( draw, turn, array )
  {
    complex_boards.forEach(function( complex_board, player )
    {
      var skip_board = complex_board
      .map( x => math.im(x) )
      .reduce(minreducer) < 0;
      if ( player+1 == 15 && turn+1 == 82) {
        console.log('before skip_board:');
        console.log(complex_board);
      }
      if ( !skip_board ) {
        if ( player+1 == 15 && turn+1 == 82) {
          console.log('after skip_board:');
          console.log(turns_encoded);
          console.log(complex_board);
        }
        var index = complex_board
        .map( x => math.re(x) )
        .findIndex( el => el == draw );
        if (index !== -1) {
          complex_board[index] = math.add(complex_board[index], math.i);
          //console.log('Turn %d. Player %d has marked number %d at index %d.', turn+1, player+1, draw, index);
          var marked_board = complex_board
          .map( x => math.im(x) );
          var turns_encoded = false;
          winning_boards.forEach(function( winboard )
          {
            if (!turns_encoded) {
              var checkboard = zipwith( (a, b) => a * b, 
              marked_board, winboard );
              if ( arrayEquals( checkboard, winboard ) )
              {
                turns_encoded = true;
                complex_board[index].im = complex_board[index].im
                *(-1)
                *(turn+1);
                if ( complex_board[index].im >= 0 ){
                  console.log('WTF why? %d should be less than 0.', complex_board[index].im);
                }
                if ( player+1 == 15 && turn+1 == 82) {
                  console.log('before draw+1 multiply:');
                  console.log('Draw is %d', draw);
                  console.log(complex_board.map( x => math.re(x) ));
                  console.log(complex_board.map( x => math.im(x) ));
                }
                complex_board = complex_board
                .map( x => 
                { 
                  if (x.im > 0){
                    return math.complex(x.re,x.im * (draw + 1));
                  }
                  else{
                    return math.complex(x.re,x.im);
                  }
                });
                if ( player+1 == 15 && turn+1 == 82) {
                  console.log('after draw+1 multiply:');
                  console.log(complex_board.map( x => math.re(x) ));
                  console.log(complex_board.map( x => math.im(x) ));
                }
              }
            }
          });
        }
      }
    });
  });

  console.log('after random order. Check board 15');
  console.log(complex_boards[14].map( x => math.im(x) ));

  complex_boards.forEach( function( complex_board, player ) {
    var turns = turns_from_board( complex_board );
    if (turns > last_turns) {
      last_winner = player;
      last_turns = turns;
      //console.log('Most turns currently by player %d with %d turns.', last_winner+1, last_turns);
    }
  });
  sorted_boards = complex_boards.sort( (board1, board2) =>
    board1
    .map( x => math.im(x) )
    .reduce(minreducer)
    -
    board2
    .map( x => math.im(x) )
    .reduce(minreducer)
  );

  /*console.log('All boards in sorted order:');
  sorted_boards.forEach( function( board )
  {
    console.log(board.map( x => math.im(x) ));
  });*/
  last_complex_board = sorted_boards[0];
  //console.log('Board that comes last:');
  //console.log(last_complex_board.map( x => math.im(x) ));
  /*last_turns = (-1) * last_complex_board
  .map( x => math.im(x) )
  .reduce(minreducer);*/
  last_draw = last_complex_board
  .map( x => math.im(x))
  .reduce(maxreducer) - 1;
  console.log('Last draw is %d. This is the board to be scored:', last_draw);
  console.log(last_complex_board.map( x => math.re(x) ));
  console.log(last_complex_board.map( x => math.im(x) ));
  last_score = calculate_score( last_complex_board, last_draw );
}


function notusedfunction( random_order, complex_boards ) {
  random_order.forEach(function( draw, turn, array )
  {
    console.log('The draw for turn %d was %d', turn+1, draw);
    complex_boards.forEach(function( complex_board, player )
    {
      console.log('Board for player %d:', player+1);
      console.log(complex_board
        .map( x => math.im(x) ));
      var skip_board = complex_board
      .map( x => math.im(x) )
      .reduce(minreducer) < 0;
      if ( skip_board ) {
        console.log('Skipping player %d', player+1);
      }
      if ( !skip_board ) {
        console.log('Player %d has not yet won.', player+1);
        var index = complex_board
        .map( x => math.re(x) )
        .findIndex( el => el == draw );
        if (index !== -1) {
          complex_board[index] = math.add(complex_board[index], math.i);
          console.log('Player %d has marked number %d at index %d.', player+1, draw, index);
          var marked_board = complex_board
          .map( x => math.im(x) );
          //console.log(complex_board.map( x => math.re(x) ));
          //console.log(marked_board);
          winning_boards.forEach(function( winboard )
          {
            //console.log(winboard);
            var checkboard = zipwith((a, b) => a * b, marked_board, winboard);
            if ( arrayEquals( checkboard, winboard ) )
            {
              console.log(checkboard);
              console.log(winboard);
              console.log('Player %d has won!', player+1);

              complex_board[index].im = complex_board[index].im
              *(-1)
              *(turn+1);
              console.log('Board for player %d:', player+1)
              console.log(complex_board.map(x => math.im(x)));
              /*console.log('All boards:');
              complex_boards.forEach( function(b, i)
              {
                console.log(b.map(x => math.im(x)));
              });
              */
              console.log(complex_board
                .map( x => math.im(x) )
                .reduce(minreducer));
              complex_board = complex_board
              .map( function(x, i)
              { 
                if (math.im(x) == 1) {
                  x.im *= (draw + 1);
                }
              });
            }
          });
          if (draw == 13 && player+1 == 2) {
            console.log('ACHTUNG:')
            console.log(complex_board.map( x => math.im(x) ));
            console.log(complex_board.map( x => math.re(x) ));
          }
        }

      }
    });
  });
  complex_boards.forEach( function( complex_board, player ) {
    //console.log( complex_board.map( x => math.re(x) ));
    
    turns = turns_from_board( complex_board );
    if (turns > last_turns) {
      last_winner = player;
      last_turns = turns;
    }
  });
  complex_boards.sort( complex_board =>
    complex_board
    .map( x => math.im(x) )
    .reduce(minreducer) );
  complex_boards.forEach( function(complex_board)
  {
    turns = complex_board
    .map( x => math.im(x) )
    .reduce(minreducer);
    //console.log(turns);
  });
  last_complex_board = complex_boards[0];
  //console.log(last_complex_board.map( x => math.re(x)));
  //console.log(last_complex_board.map( x => math.im(x)));
  last_turns = (-1) * last_complex_board
  .map( x => math.im(x) )
  .reduce(minreducer);
  last_draw = last_complex_board
  .map( x => math.im(x))
  .reduce(maxreducer) - 1;
  last_score = calculate_score( last_complex_board, last_draw );    
}
