#horizontal_positions <- read.table("C:\\Users\\Tibor\\wd\\Advent_of_Code_2021\\Day7\\simple.txt", header = FALSE, sep = ",")
#horizontal_positions <- read.table("C:\\Users\\Tibor\\wd\\Advent_of_Code_2021\\Day7\\sample.txt", header = FALSE, sep = ",")
horizontal_positions <- read.table("C:\\Users\\Tibor\\wd\\Advent_of_Code_2021\\Day7\\input.txt", header = FALSE, sep = ",")

triangularDistance <- function( a, b ) {
  n <- abs( a - b )
  return( n * ( n + 1 ) / 2 )
}

totalDistanceFrom <- function( v, m ) {
  result <- 0
  for( y in v ) {
    result <- result + abs( y - m )
  }
  return( result )
}

totalTriangularDistanceFrom <- function( v, m ) {
  result <- 0
  for( y in v ) {
    result <- result + triangularDistance( y, m )
  }
  return( result )
}

h <- unlist( horizontal_positions, use.names = FALSE )
len <- length( h )
min_hpos <- min ( h )
max_hpos <- max ( h )
p <- seq(from = min_hpos, to = max_hpos, by = 1 )

distances <- vector(,len)
i <- 1
for( y in p ) {
  distances[i] <- totalTriangularDistanceFrom( h, y )
  i <- i+1
}
#distances
min( distances )
#p
p[which.min( distances )]
