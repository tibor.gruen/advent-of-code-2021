##Read("/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day2/day2.g");

depth := 0;
horizontal_position := 0;


#sample_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day2/sample.txt";
#L := ReadCSV(sample_filepath,true,' ');

input_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day2/input.txt";
L := ReadCSV(input_filepath,true,' ');

L := List([1..Length(L)],i -> [L[i].field1,L[i].field2]);

for dv in L do
  direction := dv[1];
  value := dv[2];
  if direction = "forward" then
    horizontal_position := horizontal_position + value;
  elif direction = "down" then
    depth := depth + value;
  elif direction = "up" then
    if depth - value > 0 then
      depth := depth - value;
	else
	  Print("We are surfacing!\n");
	fi;
  else
    Error("ERROR: unknown direction\n");
  fi;
od;

Print("The final horizontal position is ",horizontal_position,"\n");

Print("The final depth is ",depth,"\n");

Print("(Multiplying these together produces ", horizontal_position * depth, ".)\n");
Print("\n");
Print("End of part 1\n\n");

#1654760

#--------- part 2 --------

aim := 0;
depth := 0;
horizontal_position := 0;

for dv in L do
  direction := dv[1];
  value := dv[2];
  if direction = "forward" then
    horizontal_position := horizontal_position + value;
	depth := depth + aim * value;
  elif direction = "down" then
    aim := aim + value;
  elif direction = "up" then
    aim := aim - value;
  else
    Error("ERROR: unknown direction\n");
  fi;
od;

Print("The final horizontal position is ",horizontal_position,"\n");

Print("The final depth is ",depth,"\n");

Print("(Multiplying these together produces ",horizontal_position*depth,".)\n");
Print("End of part 2.\n\n");
#1956047400
