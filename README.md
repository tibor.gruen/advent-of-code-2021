# Advent of Code 2021

Inspired by last year's [Solutions for Advent of Code 2020 from Tsoding](https://github.com/tsoding/aoc-2020), I try to solve as many problems from [Advent of Code 2021](https://adventofcode.com/2021) in as many programming languages I have used before at least once.
