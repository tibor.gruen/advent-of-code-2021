##Read("/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day1/day1.g");


measurements := [199,200,208,210,200,207,240,269,260,263];

print_changes := function(L)
  local i;
  for i in [1..Length(L)] do
    if i = 1 then
      Print(L[i]," (N/A - no previous measurement)\n");
    else
      if L[i] >= L[i-1] then
	    Print(L[i]," (increased)\n");
	  else
	    Print(L[i]," (decreased)\n");
      fi;
    fi;
  od;
end;

count_increases := function(L)
  local i, increases;
  increases := 0;
  for i in [2..Length(L)] do
    if L[i] >= L[i-1] then
	  increases := increases + 1;
    fi;
  od;
  return increases;
end;

print_changes(measurements);
Print("\n");
Print("There are ",count_increases(measurements)," measurements that are larger than the previous measurement.\n");

#sample_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day1/sample.txt";
#L := ReadCSV(sample_filepath,true,'\n');

input_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day1/input.txt";
L := ReadCSV(input_filepath,true,'\n');
L := List([1..Length(L)],i -> L[i].field1);

Print("There are ",count_increases(L)," measurements that are larger than the previous measurement.\n");
Print("End of part 1\n\n");

# 1557

print_sliding_window_increases := function(L)
  local len, i, previous, current;
  len := Length(L);
  current := Sum(L{[1..Minimum(len,3)]});
  for i in [1..Length(L)] do
    if i < len - 1 then
	  previous := current;
	  current := Sum(L{[i,i+1,i+2]});
      Print(current);
	  if current > previous then
	    Print(" (increasing)\n");
	  elif current < previous then
	    Print(" (decreasing)\n");
	  else
	    Print(" (no change)\n");
	  fi;
	fi;
	
  od;
end;

count_sliding_window_increases := function(L)
  local len, previous, current, increases, i;
  len := Length(L);
  increases := 0;
  current := Sum(L{[1..Minimum(len,3)]});
  for i in [2..len] do
    if i < len - 1 then
	  previous := current;
	  current := Sum(L{[i,i+1,i+2]});
	  if current > previous then
	    increases := increases + 1;
	  fi;
	fi;
  od;
  return increases;
end;

#print_sliding_window_increases(L);
Print("There are ",count_sliding_window_increases(L)," sums that are larger than the previous sum.\n");
Print("End of part 2.\n\n");

# 1608
