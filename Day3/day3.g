##Read("/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day3/day3.g");

power_consumption := 0;

#sample_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day3/sample.txt";
#L := ReadCSV(sample_filepath,true,' ');

input_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day3/input.txt";
L := ReadCSV(input_filepath,true,' ');

L := List([1..Length(L)],i -> L[i].field1);

ZerosString := function( len )
  return List( [1..len], i -> '0' );
end;

OnesString := function( len )
  return List( [1..len], i -> '1' );
end;

Pad := function( bitstring, len )
  local bitlen;
  bitlen := Length( bitstring );
  if bitlen > len then
    Error( "Bit string ", bitstring, " is longer than wanted length ", len, ".\n" );
  elif bitlen = len then
    return bitstring;
  else
    return Concatenation( ZerosString( len - bitlen ), bitstring );
  fi;
end;

bitlength := function( b )
  return LogInt( b, 10 ) + 1;
end;

max_bitlength := Maximum( List( L, bitlength ) );

L := List([1..Length(L)], i -> Pad(String(L[i]),max_bitlength));
middle := Int(Ceil(Length(L)/2.0));

gamma_rate_bin := ZerosString(max_bitlength);
epsilon_rate_bin := OnesString(max_bitlength);
position := 1;
while position <= max_bitlength do
  one_bits := Sum(List([1..Length(L)], i -> NumberDigits([L[i][position]],2)));
  if one_bits >= middle then
    gamma_rate_bin[position] := '1';
	epsilon_rate_bin[position] := '0';
  fi;
  position := position + 1;
od;

gamma_rate_dec := NumberDigits(gamma_rate_bin, 2);
epsilon_rate_dec := NumberDigits(epsilon_rate_bin, 2);

Print("The gamma rate is the binary number ",gamma_rate_bin," , or ",gamma_rate_dec," in decimal.\n");

Print("The epsilon rate is the binary number ", epsilon_rate_bin," , or ", epsilon_rate_dec," in decimal.\n");

Print("Multiplying the gamma rate (",gamma_rate_dec,") by the epsilon rate (",epsilon_rate_dec,") produces the power consumption, ",gamma_rate_dec * epsilon_rate_dec,".\n");
Print("End of part 1.\n\n");

# 4118544

## -------- part 2 ----------

oxygen_generator_rating := 0;
CO2_scrubber_rating := 0;

#sample_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day3/sample.txt";
#L := ReadCSV(sample_filepath,true,' ');

input_filepath := "/mnt/c/Users/Tibor/Dropbox/wd/wd_Gap/Advent_of_Code/Day3/input.txt";
L := ReadCSV(input_filepath,true,' ');

L := List([1..Length(L)],i -> L[i].field1);

max_bitlength := Maximum( List( L, bitlength ) );

L := List([1..Length(L)], i -> Pad(String(L[i]),max_bitlength));


filter_bit_criterium_pos := function( L, position, rating )
  local len, one_bits, zero_bits, most_common, least_common, filter;
  
  len := Length(L);
  one_bits := Sum(List([1..Length(L)], 
    i -> NumberDigits([L[i][position]],2)));
  zero_bits := len - one_bits;

  if rating = "oxygen" then
    if one_bits >= zero_bits then
      most_common := '1';
	else
	  most_common := '0';
	fi;
    return most_common;
  elif rating = "CO2" then
    if one_bits < zero_bits then
	  least_common := '1';
	else
	  least_common := '0';
	fi;
    return least_common;
  fi;
end;


oxygen := L;
for position in [1 .. max_bitlength] do
  if Length( oxygen ) > 1 then
    bit_criterium := filter_bit_criterium_pos( oxygen, position, "oxygen" );
    oxygen := Filtered( oxygen, bitstring -> bitstring[position] = bit_criterium );
  fi;
od;
oxygen := oxygen[1];

CO2 := L;
for position in [1 .. max_bitlength] do
  if Length( CO2 ) > 1 then
    bit_criterium := filter_bit_criterium_pos( CO2, position, "CO2" );
    CO2 := Filtered( CO2, bitstring -> bitstring[position] = bit_criterium );
  fi;
od;
CO2 := CO2[1];

oxygen_generator_rating := NumberDigits(oxygen, 2);

CO2_scrubber_rating := NumberDigits(CO2, 2);

Print("The oxygen generator rating is: ",oxygen,", or ", oxygen_generator_rating," in decimal.\n");

Print("The CO2 scrubber rating is: ",CO2,", or ", CO2_scrubber_rating," in decimal.\n");

life_support_rating := oxygen_generator_rating * CO2_scrubber_rating;

Print("The life support rating of the submarine is ",life_support_rating,".\n");
Print("End of Part 2.\n");

#3832770
