import java.io.*;
import java.util.*;
import java.util.function.Consumer;

class day6{

  public static <T> void forEachCurrent( List<T> list, Consumer<T> action ) {
    final int size = list.size();
    for (int i = 0; i < size; i++) {
        action.accept(list.get(i));
    }
  }

  public static void advance_timer( ArrayList<Byte> lanternfish ) {
    int len = lanternfish.size();
	for ( int i = 0; i < len; ++i ) {
	  byte fish = lanternfish.get(i);
	  if ( fish == 0 ){
	    lanternfish.set(i, (byte) 6);
        try {
          lanternfish.add((byte) 8);
        }
        catch( OutOfMemoryError e ) {
          System.out.print("ERROR: Could not add another byte. Last known length of lanternfish list was "+len+".\n");
          // sample data broke with length 224190307
          // -Xmx1g:                       144624510
          // -Xmx2g:                       224190307
          // -Xmx3g:                       346477329
          // should be length            26984457539
          // two orders of magnitude more.
          // fuck exponential growth!
		  // I need more memory!
		  //                           1592918715629
		  // way more memory!
          System.exit(1);
        }
	  }
	  else {
        fish = (byte) (fish-1);
		lanternfish.set( i, fish );
	  }
    }
  }
  
  public static int sum_fish_timers( ArrayList<Byte> lanternfish ) {
	return lanternfish.stream().reduce( (a,b) -> (byte)(a+b) ).get();
  }

  public static ArrayList<Byte> ReadFishFromFile( String filepath ) {
	FileReader fileReader;
	ArrayList<Byte> lanternfish = new ArrayList<Byte>();
	
    try {
		fileReader = new FileReader(filepath);
		int data = 0;
        byte fish = (byte) 0;
		do {
		  data = fileReader.read();
		  if ( data != ',' && data != -1 ) {
			fish = (byte) Character.getNumericValue(data);
			lanternfish.add( fish );
		  }
		}
		while( data != -1 );
	}
	catch (IOException e) {
      System.out.println("ERROR: while reading file.\n");
	  System.exit(1);
	}
	return lanternfish;
  }

  public static void PrintLanternfish( List<Byte> lanternfish, int days ) {
	if ( days == 0 ) {
	  System.out.print("Initial state: ");
	}
	else if ( days == 1 ) {
	  System.out.print("After "+days+" day:  ");
	}
	else {
	  System.out.print("After "+days+" days: ");
	}
	Iterator<Byte> fishIterator = lanternfish.iterator();
    System.out.print(fishIterator.next());
    while (fishIterator.hasNext()) {
      System.out.print(","+fishIterator.next());
    }
    System.out.print("\n");
  }
  
  public static void main( String[] args ) {
	String filepath = "C:\\Users\\Tibor\\wd\\Advent_of_Code\\Day6\\sample.txt";
	//String filepath = "C:\\Users\\Tibor\\wd\\Advent_of_Code\\Day6\\input.txt";
	ArrayList<Byte> lanternfish = new ArrayList<Byte>();
	lanternfish = ReadFishFromFile( filepath );
	// ---------------------------------------------
	int days = 0;
    PrintLanternfish( lanternfish, days );
	for( days = 1; days <= 256; days++ ) {
	  advance_timer( lanternfish );
      //PrintLanternfish( lanternfish, days );
	}
	int total = lanternfish.size();
	//int sum_timers = sum_fish_timers( lanternfish );
	System.out.print("Part 1: After "+(days-1)+" days, there are a total of "+total+" fish.\n");
  }
}