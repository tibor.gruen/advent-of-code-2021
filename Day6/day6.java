import java.io.*;
import java.util.*;
import java.util.ListIterator;
import java.util.function.Consumer;

class day6 {

  static int NEW_FISH_TIME = 8;
  static int RESET_FISH_TIME = 6;
  static int SPAWN_FISH_TIME = 0;
  
  public static <T> void forEachCurrent( List<T> list, Consumer<T> action ) {
    final int size = list.size();
    for (int i = 0; i < size; i++) {
        action.accept(list.get(i));
    }
  }

  public static ArrayList<Long> ReadFishFromFile( String filepath ) {
	FileReader fileReader;
	ArrayList<Long> lanternfish = new ArrayList<Long>(Collections.nCopies(9, (long)0));
	long fishtimed = 0;
	int fish = 0;
    try {
		fileReader = new FileReader(filepath);
		int data = 0;
		do {
		  data = fileReader.read();
		  if ( data != ',' && data != -1 ) {
			fish = Character.getNumericValue(data);
			
            fishtimed = lanternfish.get( fish ) + 1;
			try {
			  lanternfish.set( fish, fishtimed );
			}
			catch( IndexOutOfBoundsException oob ) {
			  System.out.print("ERROR: Failed to increase fish in index "+fish+". Parsed data was "+data+". Index out of bounds.\n");
			  System.exit(1);
			}
		  }
		}
		while( data != -1 );
	}
	catch (IOException e) {
      System.out.println("ERROR: while reading file.\n");
	  System.exit(1);
	}
	return lanternfish;
  }
  
  // [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
  //   0, 1, 1, 2, 1, 0, 0, 0, 0
  //   1, 1, 2, 1, 0, 0, 0, 0, 0
  //   1, 2, 1, 0, 0, 0, 1, 0, 1
  //   2, 1, 0, 0, 0, 1, 0, 1, 1
  public static ArrayList<Long> advance_timer( ArrayList<Long> lanternfish ) {
	ArrayList<Long> nextfish = new ArrayList<Long>( Collections.nCopies(9, (long)0) );
	long fishes = 0;
	long parents = 0;
	for ( int timer = 0; timer <= 8; ++timer ) {
	  fishes = lanternfish.get( timer );
	  if ( timer == 0 ) {
		parents = fishes;
        nextfish.set( 8, fishes );
	  }
	  else if ( (0 < timer) && (timer <= 6) ) {
		nextfish.set( timer - 1, fishes );
	  }
	  else if ( timer == 7 ) {
        nextfish.set( 6, (long)(parents + fishes) );
      }
	  else if ( timer == 8 ) {
		nextfish.set( timer - 1, fishes );
	  }
	}
	return nextfish;
  }
  
  public static void PrintLanternfish( ArrayList <Long> lanternfish, int days ) {
	if ( days == 0 ) {
	  System.out.print("Initial state:\n");
	}
	else if ( days == 1 ) {
	  System.out.print("After "+days+" day:\n");
	}
	else {
	  System.out.print("After "+days+" days:\n");
	}
	ListIterator<Long>fishIterator = lanternfish.listIterator();
    while (fishIterator.hasNext()) {
      System.out.print("fish timed "+fishIterator.nextIndex()+":  "+fishIterator.next() + "\n");
    }
    System.out.print("\n");
  }
  
  public static void main( String[] args ) {
	//String filepath = "C:\\Users\\Tibor\\wd\\Advent_of_Code\\Day6\\sample.txt";
	String filepath = "C:\\Users\\Tibor\\wd\\Advent_of_Code\\Day6\\input.txt";
	ArrayList<Long> lanternfish = new ArrayList<Long>();
	lanternfish = ReadFishFromFile( filepath );
	// ---------------------------------------------int days = 0;
	int days = 0;
	do {
      //PrintLanternfish( lanternfish, days );
	  if ( days >= 256 ) break;
	  lanternfish = advance_timer( lanternfish );
	  days++;
	}
	while ( true );
	long totalfishes;
	totalfishes = lanternfish.stream().reduce( (a,b) -> (long)(a+b) ).get();
	System.out.print("Part 1: After "+(days)+" days, there are "+totalfishes+" fishes in total.\n");
  }
}