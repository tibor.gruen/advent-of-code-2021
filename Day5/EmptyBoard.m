%
%
%
function board = EmptyBoard( m, n, char )
%                \n.....
%                \n.....
%                \n.....
%                \n.....
  board = blanks( 1 + (m+1) * (n+1) );
  for i = 1:m
    for j = 1:(n+1)
	  index = (i-1)*(m+1) + j;
      if ( mod( index, (m+1) ) == 1 )
        board(index) = "\n";
      else
        board(index) = char;
      endif
    endfor
  endfor
endfunction