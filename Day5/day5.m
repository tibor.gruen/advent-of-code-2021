# not a function file:
1;
clear all;
close all;

fileID = fopen('sample.txt','r');
%fileID = fopen('input.txt','r');
formatSpec = '%d,%d -> %d,%d';
sizeA = [4 Inf];
A = fscanf(fileID, formatSpec, sizeA)' + 1;
startPoints = A(:,[1,2]);
endPoints = A(:,[3,4]);
xs = A(:,[1,3]);
ys = A(:,[2,4]);
len = length(endPoints);
left = 1;
right = max(max(xs));
top = 1;
bottom = max(max(ys));

m = right - left + 1;
n = bottom - top + 1;
%cd("C:\\Users\\Tibor\\Dropbox\\wd\\wd_Octave\\numFun");
%fprintf("Empty board of size m = %d, n = %d:\n", m, n );
board = EmptyBoard( m, n, "." );
%fprintf("INFO: Length of board = %d.\n", length(board) );
assert(length(board) == m * (n+2) + 2);
%display(board)
for i = 1:len
  [x1, y1] = deal( num2cell(startPoints(i,:)){:});
  [x2, y2] = deal( num2cell(endPoints(i,:)){:});
  if ( x1 == x2 ) % vertical
    if y2 < y1 [y1, y2] = swap(y1, y2); endif
    for y = y1:y2
	  [ board, stateIP ] = InsertPoint( x1, y, board, m, n );
	  if (!stateIP)
	    fprintf("ERROR: Could not insert point at (%d, %d).\n", x1, y );
	  endif
	endfor
  elseif ( y1 == y2 ) % horizontal
    if x2 < x1 [x1, x2] = swap(x1, x2); endif
    for x = x1:x2
	  [ board, stateIP ] = InsertPoint( x, y1, board, m, n );
	  if (!stateIP)
	    fprintf("ERROR: Could not insert point at (%d, %d).\n", x, y1 );
	  endif;
	endfor
  elseif ( (y2 - y1) == (x2 - x1) ); % diagonal /
    if ( x2 < x1 )
	  [ x1, x2 ] = swap( x1, x2 );
	endif
	if ( y2 < y1 )
	  [ y1, y2 ] = swap( y1, y2 );
	endif
	assert( x1 < x2 );
	assert( y1 < y2 );
	for t = 0:(x2-x1)
	  x = x1 + t;
	  y = y1 + t;
	  [ board, stateIP ] = InsertPoint( x, y, board, m, n );
	  if (!stateIP)
	    fprintf("ERROR: Could not insert point at (%d, %d).\n", x, y1 );
	  endif;
	endfor
  elseif ( (y2 - y1) == -(x2 - x1) ); % diagonal \
	if ( x2 < x1 )
      [ x1, x2 ] = swap( x1, x2 );
	endif
	if ( y2 > y1 )
	  [ y1, y2 ] = swap( y1, y2 );
	endif
	assert( x1 < x2 );
	assert( y1 > y2 );
	for t = 0:(x2-x1)
	  x = x1 + t;
	  y = y1 - t;
	  [ board, stateIP ] = InsertPoint( x, y, board, m, n );
	  if (!stateIP)
	    fprintf("ERROR: Could not insert point at (%d, %d).\n", x, y );
	  endif;
	endfor
    
  endif
endfor

%display(board)

dangerpoints = 0;
for i = 1:m
  for j = 1:(n+1)
	index = (i-1)*(m+1) + j;
    if ( mod( index, (m+1) ) != 1 )
	  if (board(index) != "." && board(index) != "1")
	    dangerpoints = dangerpoints + 1;
	  endif
	endif
  endfor
endfor

dangerpoints

fclose(fileID);
%cd("C:\\Users\\Tibor\\wd\\Advent_of_Code\\Day5");