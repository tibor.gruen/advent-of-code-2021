%
%
%
function [ board, stateIP ] = InsertPoint( i, j, board_, m, n )
  board = board_;
  stateIP = true;
  len = length(board);
  index = (j-1)*(m+1) + i + 1;
  if ( mod( index, (m+1) ) == 1 )
    display( board(index) );
  endif
  if index > len
    fprintf("ERROR: Index %d out of bounds. Length of board is only %d.\n", index, len);
	stateIP = false;
  endif
  switch ( board(index) )
    case "\n"
      display("ERROR: Could not change newline character.\n");
	  fprintf( "INFO: index was %d, i = %d, j = %d.\n", index, i, j );
	  stateIP = false;
    case "."
      board(index) = "1";
    case "X"
      board(index) = "X";
	otherwise
	  str = board(index);
	  assert( length(str) == 1);
	  [ point, stateSN ] = str2num( str );
	  if ( !stateSN )
	    fprintf("Error: could not convert string %s to number.\n", str );
		stateIP = false;
	  endif
	  if ( point == 9 )
	    board(index) = "X"
	  elseif ( (1 <= point) && (point <= 8) )
	    point = point + 1;
		str = num2str(point);
	    board(index) = str;
	  else
	    
	  endif
  endswitch
endfunction